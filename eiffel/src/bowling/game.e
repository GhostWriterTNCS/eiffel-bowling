note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

create
    make

feature
	scores: ARRAY [INTEGER]
	score_bonus: INTEGER

	frame_index: INTEGER
	roll_index: INTEGER
	bonus_remains: INTEGER

	make
		do
			--scores.make_filled(0, 0, 20) -- := << 0, 0, 0 >>
			create scores.make_filled (0, 1, 21)

            print("Hello World%N")
        end

	score: INTEGER
		local
			score_value: INTEGER
		do
			across scores as s loop score_value := score_value + s.item end
			result := score_value
		end

	roll (pins: INTEGER)
		require
			pins >= 0 and pins <= 10
		do
			scores.put(pins, frame_index+roll_index)
			if roll_index = 0 then
				if pins = 10 then
					frame_index := frame_index + 1
					bonus_remains := 2
				else
					roll_index := 1
				end
			else
				if frame_index = 10 and bonus_remains >= 1 then
					roll_index := roll_index + 1
				else
					roll_index := 0
					frame_index := frame_index + 1
				end
			end
		end

	rollMany (pins: INTEGER; times: INTEGER)
		require
			pins >= 0 and pins <= 10
		local i: INTEGER
		do
			from i := 0 until i < times loop
				roll(pins)
			end
		end
end
